# GOF - Criacional - Prototype
Este projeto tem o intuito de estudar o padrão `Prototype` do grupo `Criacional` dos padrões de projeto GOF.

[Referência](https://refactoring.guru/pt-br/design-patterns/prototype)

Neste projeto foi implementado uma `lista das cidades do estado referente a determinado CEP`. Como a criação do `item` 
inicial da lista é feito através de uma chamada a uma API externa, criar os demais itens com a mesma inicialização pode 
ser muito demorado, então os demais itens são `clonados`. 

# Docker
## Criar a imagem
```bash
docker-compose build --no-cache # Apenas uma vez
```

## Entrar no terminal do container
```bash
docker-compose run --rm workspace bash
```

# Instalar dependências composer
```bash
composer install
```

# Exemplos
```bash
php exemplos/app.php
```
> Você será solicitado a fornecer um CEP, informe um CEP válido e pressione `ENTER`.

# Testes Automatizados
```bash
bin/phpunit --debug --coverage-text
```