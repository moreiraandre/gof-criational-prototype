#!/usr/bin/env php
<?php

require 'vendor/autoload.php';

use GuzzleHttp\Client as ClientHttp;

$resposta = readline('Informe o CEP desejado para obter a lista de cidades: ');

$primeiraCidade = new ItemCidade($resposta);
$listaCidades = [$primeiraCidade];

$clientHttp = new ClientHttp(['base_uri' => 'https://servicodados.ibge.gov.br/api/v1/localidades/estados/']);
$respostaHttp = $clientHttp->request('GET', "$primeiraCidade->uf/municipios");
$cidades = json_decode($respostaHttp->getBody()->getContents());
foreach ($cidades as $cidade) {
    if ($cidade->nome == $primeiraCidade->nomeCidade) {
        continue;
    }

    $novaCidade = clone $primeiraCidade;
    $novaCidade->nomeCidade = $cidade->nome;
    $listaCidades[] = $novaCidade;
}

file_put_contents('./exemplos/tmp/cidades.json', json_encode($listaCidades));

echo 'Arquivo gerado ./exemplos/tmp/cidades.json' . PHP_EOL;
