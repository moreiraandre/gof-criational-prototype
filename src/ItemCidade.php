<?php

use GuzzleHttp\Client as ClientHttp;

class ItemCidade
{
    public string $nomeCidade;
    public readonly string $uf;
    private bool $isPrimeiroObjeto = true;

    public function __construct(string $cep)
    {
        $clientHttp = new ClientHttp(['base_uri' => 'https://viacep.com.br/ws/']);
        $resposta = $clientHttp->request('GET', "$cep/json/");
        $respostaDados = json_decode($resposta->getBody()->getContents());

        $this->nomeCidade = $respostaDados->localidade;
        $this->uf = $respostaDados->uf;
    }

    public function __clone(): void
    {
        $this->isPrimeiroObjeto = false;
    }

    public function isPrimeiroObjeto(): bool
    {
        return $this->isPrimeiroObjeto;
    }
}