<?php

namespace Tests\Feature;

use ItemCidade;
use PHPUnit\Framework\TestCase;

class SucessoTest extends TestCase
{
    public function testSucesso()
    {
        $primeiraCidade = new ItemCidade('69301-020');
        $cidadeClonada = clone $primeiraCidade;

        $this->assertTrue($primeiraCidade->isPrimeiroObjeto());
        $this->assertFalse($cidadeClonada->isPrimeiroObjeto());
    }
}